#!/bin/bash

# As of 9/25, current time just for human readability while configuring
#10:00am switch to 20m - 2:30 after sunrise?
#10:00pm switch to 40m - 2:15 after sunset?

twenty=2.5
fourty=2.25

# Script to change radio
rigtool="${HOME}/baofeng/bin/rigtool/rigtool.pl"
# Script with vars for sunrise, sunset, etc
sundata="${HOME}/.sun.sh"
# Flag for determining if daylight status has changed since last run
dayflag="${HOME}/.dayflag"
# Log file
logfile="${HOME}/.freqswitch.log"

# Determine if it's daylight
function suneval
{
    # How much to adjust time of day required to be considered "night"
    # Positive numbers make it later in the day, 8pm with 2 would be 10pm
    adjust=$1

    # Default to being daytime
    daylight=1

    # Get the sr/ss values
    source ${sundata}

    # Get the current time in decimal
    curhour=`date +'%H'`
    curmin=`date +'%M'`
    curmindec=`echo "scale=3;${curmin}/60" | bc`
    curdec=`echo "${curhour} + ${curmindec}" | bc`
    
    # Compare sunrise/sunset against current time with bc since bash is retarded
    srcomp=`echo "${curdec} < (${srdec} + ${adjust})" | bc -l`
    sscomp=`echo "${curdec} > (${ssdec} + ${adjust})" | bc -l`

    # Finally; determine if it's dark
    if [ ${srcomp} == 1 ]; then
        daylight=0
    elif [ ${sscomp} == 1 ]; then
        daylight=0
    fi

#   echo "sr [${srdec}] ss [${ssdec}] cur [${curdec}] src [${srcomp}] ssc [${sscomp}] adj [${adjust}] daylight [${daylight}] lastdl [${lastdaylight}]" >> ${logfile}
}

# If our flag file exists, source it, otherwise put nonsense in test var
if [ -e ${dayflag} ]; then
    source ${dayflag}
else
    lastdaylight="x"
fi

# Temporary; logging timestamp
ts=`date +"%Y-%m-%d_%H-%M-%S"`

# Get the hour so we evaluate 20 only in morning, 40 in evening
# Otherwise if their offsets vary, it may flip-flop it being daylight
hour=`date +"%H"`

# Test for 20m; we want to switch to it 2 hours after sunrise,
# only evaluate in the morning
if [ ${hour} -lt 12 ]; then
    suneval ${twenty}
    # If our result is the same as last time, exit
    if [ "${daylight}" == "1" ] && [ "${daylight}" != "${lastdaylight}" ]; then
        echo "$ts: Switching to 20m" >> ${logfile}
        echo "sr [${srdec}] ss [${ssdec}] cur [${curdec}] src [${srcomp}] ssc [${sscomp}] adj [${adjust}] daylight [${daylight}] lastdl [${lastdaylight}]" >> ${logfile}
        echo "lastdaylight=1" > ${dayflag}
        DISPLAY=:0 notify-send "Switching to 20 meters" --icon=audio-speakers -t 3000
        ${rigtool} 14074 &> /dev/null
        ${rigtool} tuner off &> /dev/null
    fi
fi

# Test for 40m; 2.5 hours after sunset, only evaluate in evening
if [ ${hour} -gt 12 ]; then
    suneval ${fourty}
    if [ "${daylight}" == "0" ] && [ "${daylight}" != "${lastdaylight}" ]; then
        echo "$ts: Switching to 40m" >> ${logfile}
        echo "sr [${srdec}] ss [${ssdec}] cur [${curdec}] src [${srcomp}] ssc [${sscomp}] adj [${adjust}] daylight [${daylight}] lastdl [${lastdaylight}]" >> ${logfile}
        echo "lastdaylight=0" > ${dayflag}
        DISPLAY=:0 notify-send "Switching to 40 meters" --icon=audio-speakers -t 3000
        ${rigtool} 7074 &> /dev/null
        ${rigtool} tuner off &> /dev/null
    fi
fi