#!/usr/bin/perl

# Perl script to control a radio using HamLib

use Hamlib;
use Term::ReadKey;
use strict;
use warnings;
use FindBin;
use lib $FindBin::Bin;

use Data::Dumper;
our $VERSION = '1.1';

# Todo - Remote tuning
# Turn off tuner automatically, but when?  When on known channel with ATU and NOT scanning?  

# Use our rigtool.pm module and get all of our settings out of it
use rigtool;

my $hamliburl          = $rigtool::hamliburl;
my $country            = $rigtool::country;
my $license            = $rigtool::license;
my $cw_passband        = $rigtool::cw_passband;
my $data_passband      = $rigtool::data_passband;
my $ssb_passband       = $rigtool::ssb_passband;
my $am_passband        = $rigtool::am_passband;
my $fm_passband        = $rigtool::fm_passband;
my $tune_bottom        = $rigtool::tune_bottom;
my $tune_top           = $rigtool::tune_top;
my $enforce_tune_limit = $rigtool::enforce_tune_limit;
my $cwoffset           = $rigtool::cwoffset;
my $fautomodeset       = $rigtool::fautomodeset;
my $bypassdatamode     = $rigtool::bypassdatamode;
my $allmodeset         = $rigtool::allmodeset;
my $avgsignal          = $rigtool::avgsignal;
my $avgsamples         = $rigtool::avgsamples;
my $showtuneinfo       = $rigtool::showtuneinfo;
my $showbandinfo       = $rigtool::showbandinfo;
my $locked             = $rigtool::locked;
my $rigopenmax         = $rigtool::rigopenmax;
my $hzstep             = $rigtool::hzstep;
my $khzstep            = $rigtool::khzstep;
my $fivekhzstep        = $rigtool::fivekhzstep;
my $largestep          = $rigtool::largestep;
my $hzupkey            = $rigtool::hzupkey;
my $hzdownkey          = $rigtool::hzdownkey;
my $khzupkey           = $rigtool::khzupkey;
my $khzupkeytwo        = $rigtool::khzupkeytwo;
my $khzdownkey         = $rigtool::khzdownkey;
my $khzdownkeytwo      = $rigtool::khzdownkeytwo;
my $fivekhzupkey       = $rigtool::fivekhzupkey;
my $fivekhzdownkey     = $rigtool::fivekhzdownkey;
my $largeupkey         = $rigtool::largeupkey;
my $largedownkey       = $rigtool::largedownkey;
my $powerupkey         = $rigtool::powerupkey;
my $powerdownkey       = $rigtool::powerdownkey;
my $powerinc           = $rigtool::powerinc;
my $scanstep           = $rigtool::scanstep;
my $scandelay          = $rigtool::scandelay;
my $scanupkey          = $rigtool::scanupkey;
my $scandownkey        = $rigtool::scandownkey;
my $coloroutput        = $rigtool::coloroutput;
my $lightterm          = $rigtool::lightterm;
my @sixtymfreqs        = @rigtool::sixtymfreqs;
my %tuneinfo           = %rigtool::tuneinfo;
my %freqnames          = %rigtool::freqnames;
my %bandnames          = %rigtool::bandnames;
my %powerfreqs         = %rigtool::powerfreqs;
my $notefile           = $rigtool::notefile;
my $powerdivider       = $rigtool::powerdivider;
my $swr_multiplier     = $rigtool::swr_multiplier;
my $swr_constant       = $rigtool::swr_constant;
my $swr_testing        = $rigtool::swr_testing;
my $swr_logging        = $rigtool::swr_logging;
my $swrfile            = $rigtool::swrfile;
my $autodelay          = $rigtool::autodelay;
my $smeter             = $rigtool::smeter;
my $smetergraph        = $rigtool::smetergraph;
my $autopower          = $rigtool::autopower;
my $manualpower        = $rigtool::manualpower;

# Comment out to enable hamlib debugging (very noisy)
Hamlib::rig_set_debug($Hamlib::RIG_DEBUG_NONE);

# Change to your rig type, port, etc
my $rig = Hamlib::Rig->new($Hamlib::RIG_MODEL_NETRIGCTL);
$rig->set_conf( '', $hamliburl );

# Don't touch below here unless modifying %tuneinfo
# if you want to modify that

# Define our cw and ssb freqs and get them defined based on license
# The data is pulled from the get_bandplan sub in our module
my ( $cwfreqsref, $datafreqsref, $ssbfreqsref )
    = &rigtool::get_bandplan( $country, $license );
my @cwfreqs   = @$cwfreqsref;
my @datafreqs = @$datafreqsref;
my @ssbfreqs  = @$ssbfreqsref;

# Auto mode flag to use in various places
my $automode = '0';

# Scan mode flag
my $scanmode = '0';

# Keep track of average
my @signalarray;
my $avgtimes = '0';
my $lastavg  = '-99';

# Track what mode we were in last so we don't switch to the same mode
my $lastmode = '';

# Keep track of last vfo used so we can use it again
my $lastvfo = '';

# Keep track of our last input so we can use a repeat command later
my $lastinput = '';

# Keep track of our last channel so we can rotate through them
my $lastchannel = '';

# Check to see if we have the Term::ANSIColor module so it's not a hard requirement
my $ansi = eval {
    require Term::ANSIColor;
    Term::ANSIColor->import(':constants');
    1;
};

# Set up colors and other ANSI codes
my ( $r, $c_r, $c_g, $c_c, $c_m, $c_b, $c_y, $c_w, $cl, $bold,
    $clearrestscreen, $clearscreen, $topleft );

# Set up our color tags
color_tags();

# Keep track of how many times we had to open the connection for info
my $rigopens = '0';

rigopen();

# Loop vars defined so we can control exiting better
my $whileloop = '1';
my $autoloop  = '0';

# Frequency divider to get to khz
my $freqdiv = '1000';

# Variables so we can flip/flop frequencies or return to the last one easy
my $quickfreq = $rig->get_freq() / $freqdiv;
my $quickmode = 'u';
my $lastfreq  = $quickfreq * $freqdiv;

# Set the lastpower variable on startup
my $lastpower = get_power();
# Flag that's set to 1 if we're on a frequency matching our powerfreqs hash
my $freqpower = '0';

# Track the last SWR
my $lastswr = 'NA';
my $lastswrfreq = '0';

# Track the last signal
my $signal;

# If auto is the first parameter, go straight to auto mode

my $arg = '';

if ( scalar(@ARGV) ) {
    $arg = join(' ', @ARGV);
}

# If auto, enter auto mode, otherwise attempt to parse as a single command
if ( $arg eq 'auto' ) {
    $autoloop = '1';
    auto_mode();
}
elsif ( $arg ne '' )
{
    parse_input($arg);
    exit;
}

# Start manual loop
manual_mode();

print "Exiting script\n";

rigclose();

exit;

sub manual_mode {
    # Loop through our prompt while we're running
    while ($whileloop) {
        my ( $prompt, $tunertext, $extratext, $bandtext, $chantext )
            = create_prompt();

        if ( $tunertext ne '' ) {
            print $tunertext . ' ' . $bandtext . $chantext . "\n";
        }
        print $prompt . ': ';
        my $input = <STDIN>;
        if ( defined($input) && $input )
        {
            chomp $input;
            $input = ($input);
            parse_input($input);
        }
        else
        {
            print "\n";
        }
        # Return to our locked freq/mode if locked
        if ($locked) {
            parse_f($quickfreq);
            parse_mode($quickmode);
        }

        check_freq_power() if $manualpower;
    }
    return;
}

# Clean up
sub freq_text {
    my ($passedfreq) = @_;

    no warnings 'uninitialized';

    my $cwmatch   = '0';
    my $datamatch = '0';
    my $ssbmatch  = '0';
    my $matched   = '';
    my $tunertext = '';
    my $bandtext  = '';
    my $chantext  = '';
    my $testfreq  = '1000';
    my $f         = $testfreq;

    # If we were passed a frequency, find the info for it, not the tuned one
    if ( defined($passedfreq) ) {
        $f = $passedfreq;
    }
    else {
        $f = $rig->get_freq();
    }

    # Sometimes we seem to get nonsense, try to cycle the connection
    if ( $f =~ /NaN/ || $f < $testfreq ) {
        rigclose();
        rigopen();
        my %returnhash = (
            'pretty_freq' => 0,
            'cwmatch'     => 0,
            'datamatch'   => 0,
            'ssbmatch'    => 0,
            'outofband'   => 0,
            'tunertext'   => '',
            'bandtext'    => '',
            'chantext'    => ''
        );
        return %returnhash;
    }
    else {
        # Reset our rigopens counter if we got a good value
        $rigopens = 0;
    }

    foreach my $item (@cwfreqs) {

        # Test if we have a range
        if ( $item =~ /-/xms ) {
            my ( $low, $high ) = split( /-/xms, $item );
            if ( $f >= $low && $f < $high ) {
                $cwmatch = '1';
            }
        }
        else

            # Single frequency; ie:  60M
        {
            if ( $f == $item ) {
                $cwmatch = '1';
            }
        }
    }

    foreach my $item (@datafreqs) {

        # Test if we have a range
        if ( $item =~ /-/xms ) {
            my ( $low, $high ) = split( /-/xms, $item );
            if ( $f >= $low && $f < $high ) {
                $datamatch = '1';
            }
        }
        else

            # Single frequency; ie:  60M
        {
            if ( $f == $item ) {
                $datamatch = '1';
            }
        }
    }

    foreach my $item (@ssbfreqs) {

        # Test if we have a range
        if ( $item =~ /-/xms ) {
            my ( $low, $high ) = split( /-/xms, $item );
            if ( $f >= $low && $f < $high ) {
                $ssbmatch = '1';
            }
        }
        else

            # Single frequency; ie:  60M
        {
            if ( $f == $item ) {
                $ssbmatch = '1';
            }
        }
    }

    # If set, add tuner info to prompt text
    if ($showtuneinfo) {
        for my $key ( keys %tuneinfo ) {
            my $value = $tuneinfo{$key};

            my ( $low, $high ) = split( /-/xms, $key );
            if ( $f >= $low && $f <= $high ) {
                $tunertext = '(' . $value . ')';
            }
        }
    }

    # If set, add band info to prompt text
    if ($showbandinfo) {
        for my $key ( keys %bandnames ) {
            my $value = $bandnames{$key};

            my ( $low, $high ) = split( /-/xms, $key );
            if ( $f >= $low && $f < $high ) {
                $bandtext = '(' . $value . ')';
            }
        }
    }

    if ( $cwmatch || $datamatch || $ssbmatch ) { $matched = '1'; }

    # Pad the frequency to make it look nicer
    my $pretty_freq = $f / $freqdiv;
    my ( $khz, $hz );
    if ( $pretty_freq =~ /\./xms ) {
        ( $khz, $hz ) = split( /\./xms, $pretty_freq );
        my $padlength = 2 - length $hz;
        if ( $padlength > 0 ) {
            $hz .= '0' x $padlength;
        }

        # Otherwise, just pad two zeros
        else {
            $hz .= '00';
        }
        $pretty_freq = $khz . '.' . $hz;
    }
    else {
        $pretty_freq = $pretty_freq . '.00';
    }
    my $outofband = '0';
    unless ( $matched || !defined($license) ) { $outofband = '1'; }

    # If we know this frequency, add it to the prompt
    my $freqname = name_from_freq($f);
    if ( defined($freqname) && $freqname ne '' ) {
        $chantext .= ' (' . $c_c . $freqname . $r . ') ';
    }

# Create a hash to return so it makes it easier to extend and only use what we need
    my %returnhash = (
        'pretty_freq' => $pretty_freq,
        'cwmatch'     => $cwmatch,
        'datamatch'   => $datamatch,
        'ssbmatch'    => $ssbmatch,
        'outofband'   => $outofband,
        'tunertext'   => $tunertext,
        'bandtext'    => $bandtext,
        'chantext'    => $chantext
    );

    return %returnhash;
}

sub parse_input {
    my ($orig_input) = @_;

    # Lowercase our input for most tests
    my $input = lc($orig_input);

    my $storelastinput = '0';
    my $skip           = '0';

    # Get current info so we can switch back to these if we change with 'r'
    my $f = $rig->get_freq();
    my ( $mode, $width ) = $rig->get_mode();
    my ( $textmode, @rest ) = split( //xms, Hamlib::rig_strrmode($mode) );

    # Repeat last command

    if ( $input eq '!' ) {
        $input = $lastinput;
        print 'Repeating command:  ' . $lastinput . "\n";
    }

    # Help
    if ( $input =~ /\?+/xms ) {
        usage($input);
    }

    # Automatic mode
    if ( $input =~ /auto/xms ) {
        $autoloop = '1';
        auto_mode();
        $input = '';
        $skip++;
    }

    # Lock to current freq/mode
    if ( $input =~ /^lock/xms ) {
        print "Locking\n";
        $locked    = '1';
        $quickfreq = $f / $freqdiv;
        $quickmode = $textmode;
        $input     = '';
        $skip++;
    }

    # Unlock
    if ( $input =~ /^unlock/xms ) {
        print "Unlocking\n";
        $locked = '0';
        $input  = '';
        $skip++;
    }

    if ( $input =~ /q/xms ) {
        $whileloop = '0';
        return;
    }

    if ( $input =~ /set/xms ) {
        my ( undef, $data )    = split( /\ /xms, $input );
        my ( $var,  $setting ) = split( /=/xms,  $data );
        change_setting( $var, $setting );
        return;
    }

    # Change radio power
    if ( $input =~ /^power\ (\d+).*$/xms ) {
        my $data = $1;
        if ( defined($data) ) {
            if ( $data > 0 && $data <= 1500 ) {
                my $outputpower = get_power();
                print 'Setting power from '
                    . $outputpower . ' to '
                    . $data
                    . " watts\n";

                my @powerdata = ( $data, 1 );
                set_power(@powerdata);
            }
            else {
                print 'Invalid power value, must be between 0 and 1500'
                    . "\n";
            }
        }
        else {
            print 'No power value, must be between 0 and 1500' . "\n";
        }
        return;
    }

    if ( $input =~ /tuner\ (.*)/xms ) {
        my $string = tuner($1);
        print "$string\n";
        return;
    }

    if ( $input =~ /note\ (.*)/xms ) {
        my $notestring = $1;
        save_note($notestring);
        return;
    }

    # Do nothing if we're locked
    if ($locked) { return; }

    if ( $input =~ /chan\ (.*)/xms ) {
        my $channel = $1;
        my ( $freq, $chanmode ) = freq_from_name($channel);
        if ( defined($freq) && $freq != 0 ) {
            parse_f($freq);
            if ( $chanmode ne "" ) {
                parse_mode($chanmode);
            }
            $storelastinput++;
        }
        $skip++;
    }

# Skip our other tokens if we've done something that might execute more commands
    if ( !$skip ) {

        # Revert to previous mode
        if ( $input =~ /r/xms ) {

            # Switch modes
            parse_f($quickfreq);
            parse_mode($quickmode);
        }

        # Change to VFO A
        if ( $input =~ /a/xms && $input !~ /am/xms ) {
            unless ( $automode || $scanmode ) {
                print "Switching to VFO A\n";
            }

            # If we specify vfo, make sure we honor it
            $lastvfo = 'A';
            $rig->set_vfo($Hamlib::RIG_VFO_VFO);
            $rig->set_vfo($Hamlib::RIG_VFO_A);
        }

        # Change to VFO B
        if ( $input =~ /b/xms ) {
            unless ( $automode || $scanmode ) {
                print "Switching to VFO B\n";
            }

            # If we specify vfo, make sure we honor it
            $lastvfo = 'B';
            $rig->set_vfo($Hamlib::RIG_VFO_VFO);
            $rig->set_vfo($Hamlib::RIG_VFO_B);
        }

        # Parse and change frequency
        if ( ( $input =~ /f\ *\d+/xms && $input !~ /fm/xms )
            || $input =~ /^[0-9\.,]+$/xms )
        {
            print "input: $input\n";
            parse_f($input);
            $storelastinput++;
        }

        # Parse and change mode
        if (   $input =~ /u/xms
            || $input =~ /l/xms
            || $input =~ /c/xms
            || $input =~ /am/xms
            || $input =~ /fm/xms
            || $input =~ /d/xms )
        {
            parse_mode($input);
        }

        # Scan up
        if ( $input =~ /sup/xms ) {
            scan( $f / $freqdiv, 0, 'up' );
            $storelastinput++;
        }

        # Scan down
        if ( $input =~ /sdown/xms ) {
            scan( $f / $freqdiv, 0, 'down' );
            $storelastinput++;
        }

        # Scan range
        if ( $input =~ /s\ *(\d+)\-(\d+)/xms ||
            $input =~ /s\ *(\d+)\ (\d+)/xms ) {
            my $bottom = $1;
            my $top = $2;
            $bottom =~ s/s//gx;
            if ( $top > $bottom )
            {
                # Trim any whitespace out
                scan( $bottom, $top, '' );
                $storelastinput++;
            }
            else {
                print "Invalid scan range.  Example:  s7200-7300 or s7200 7300\n";
            }
        }

        # If empty enter key, see if we changed frequency
        if ( $input =~ /^$/xms ) {
            if ( $f != $lastfreq ) {
                my $freqname = name_from_freq($f);

                my %freqtext = freq_text();

                if (   $freqtext{pretty_freq} !~ /NaN/
                    && $freqtext{pretty_freq} != 0 )
                {
                    print 'Switched to ' . $freqtext{pretty_freq} . ' kHz';
                    if ( defined($freqname) && $freqname ne '' ) {
                        print ' (' . $c_c . $freqname . $r . ')';
                    }
                    print "\n";
                }
                $lastfreq = $f;

                # We've changed frequency, clear out lastswr
                $lastswr = 'NA';
            }
            return;
        }
    }

    # Store our quick settings to switch back to later
    unless ($locked) {
        $quickfreq = $f / $freqdiv;
        $quickmode = $textmode;
    }

# Store our last command for our ! repeat command if we passed a valid command
    if ($storelastinput) {
        $lastinput = $input;
    }

    return;
}

# Change mode based on current frequency
sub auto_mode {
    no warnings 'uninitialized';

    $automode = '1';
    ReadMode('cbreak');

    print $clearscreen;

    # Set up the variables we populate in the init section on idle
    my ($char, $extra, $f, $tunertext, $extratext, $prompt, $mode, $width, $freqname, $bandtext, $textmode) = '';
    my %freqtext;

    # Initialize our prompt the first time through
    my $init = '1';

    # ptt status flag
    my $ptt = '0';

    while ($autoloop) {
        # If init is 1 we need to set up the prompt before doing anything
        if ( $init == 0 )
        {
            ReadMode('cbreak');
            # Read twice here, so we get at least two characters of the input
            my $charone = ReadKey(-1);
            my $chartwo = ReadKey(-1);

            # We need this defined, so if it's not, make it so
            if ( !defined($charone) ) {
                $charone = '';
            }

            if ( !defined($chartwo) ) {
                $chartwo = '';
            }

            # Combine the two so we get more unique character input
            $char = $charone . $chartwo;

            # Create a UTC timestamp
            my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime();
            my $timestring = sprintf ("%02s:%02s:%02s", $hour, $min, $sec);

            print $topleft . $cl . '[' . $c_y . $timestring . $r . '] ';

            print $tunertext . "\n";

            print $cl . $extratext . "\n";

            print $cl . $prompt;

            if ( $smeter )
            {
                my $metertext = smeter( $signal );
                print "\n" . $cl . $metertext . "\n";
            }
            else
            {
                print "\n";
            }

            # Causes excessive refreshes, but need more testing
            # Replaced this with $cl in line 683
            #print $topleft . $clearrestscreen;
            
            # Exit auto mode
            if ( $char eq 'q' ) { $autoloop = '0'; }

            # Exit script completely
            if ( $char eq 'Q' ) {
                $autoloop  = '0';
                $whileloop = '0';
            }

            # Enable lock mode
            if ( $char eq 'l' ) {

                # Lock to current freq/mode
                $locked    = '1';
                $quickfreq = $f / $freqdiv;
                $quickmode = $textmode;
            }

            # Disable lock mode
            if ( $char eq 'u' ) {
                $locked = '0';
            }

            # Control PTT
            if ( $char =~ /\ / )
            {
                my $vfo = $rig->get_vfo();
                if ( $ptt )
                {
                    $rig->set_ptt($vfo,$Hamlib::RIG_PTT_OFF);
                    $ptt = '0';
                }
                else
                {
                    $rig->set_ptt($vfo,$Hamlib::RIG_PTT_ON);
                    $ptt = '1';
                }
            }

            # Parse and change frequency
            if ( $char =~ /f/ )
            {
                print $topleft . $clearrestscreen;
                print "Input frequency: ";
                ReadMode('normal');
                my $input = <STDIN>;
                parse_f($input);
            }

            # Change power
            if ( $char =~ /p/ )
            {
                print $topleft . $clearrestscreen;
                print "Input power: ";
                ReadMode('normal');
                my $input = <STDIN>;
                chomp($input);
                $input =~ s/w//i;
                if ( $input > 0 && $input <= 1500 ) {
                    my @powerdata = ( $input, 1 );
                    set_power(@powerdata);
                }
            }

            # Toggle tuner
            if ( $char =~ /t/ )
            {
                tuner('toggle');
            }

            # Tune
            if ( $char =~ /T/ )
            {
                tuner('tune');
            }
            
            # Capture a note for the current frequency
            if ( $char =~ /n/ ) {
                print $topleft . $clearrestscreen;
                print "Input note: ";
                ReadMode('normal');
                my $notestring = <STDIN>;
                save_note($notestring);
            }

            # Process arrow keys, dunno why I can't seem to read the whole input
            # so just look for what the ANSI code has in it
            my $tmpf = $f / $freqdiv;

            # Up 10hz
            if ( $char eq $hzupkey ) {
                $tmpf += $hzstep;
                $f = parse_f($tmpf);
            }

            # Down 10hz
            if ( $char eq $hzdownkey ) {
                $tmpf -= $hzstep;
                $f = parse_f($tmpf);
            }

            # Up 1khz
            if ( $char eq $khzupkey || $char eq $khzupkeytwo ) {
                $tmpf += $khzstep;
                $f = parse_f($tmpf);
            }

            # Down 1khz
            if ( $char eq $khzdownkey ||  $char eq $khzdownkeytwo ) {
                $tmpf -= $khzstep;
                $f = parse_f($tmpf);
            }

            # Up 5khz
            if ( $char eq $fivekhzupkey ) {
                $tmpf += $fivekhzstep;
                $f = parse_f($tmpf);
            }

            # Down 5hz
            if ( $char eq $fivekhzdownkey ) {
                $tmpf -= $fivekhzstep;
                $f = parse_f($tmpf);
            }

            # Up 10khz
            if ( $char eq $largeupkey ) {
                $tmpf += $largestep;
                $f = parse_f($tmpf);
            }

            # Down 10khz
            if ( $char eq $largedownkey ) {
                $tmpf -= $largestep;
                $f = parse_f($tmpf);
            }

            # Scan up
            if ( $char eq $scanupkey ) {
                scan( $tmpf, 0, 'up' );
            }

            # Scan down
            if ( $char eq $scandownkey ) {
                scan( $tmpf, 0, 'down' );
            }

            # Power up
            if ( $char eq $powerupkey ) {
                my $currentpower = get_power();
                $currentpower += $powerinc;
                set_power($currentpower);
            }

            # Power down
            if ( $char eq $powerdownkey ) {
                my $currentpower = get_power();
                $currentpower -= $powerinc;
                set_power($currentpower);
            }

            # Return to our locked freq/mode if locked
            if ($locked) {
                parse_f($quickfreq);
                parse_mode($quickmode);
            }

            # If frequency has changed, reset lastswr
            if ($lastfreq != $f)
            {
                $lastswr = 'NA';
            }

            # Set lastfreq to our last known frequency
            $lastfreq = $f;
        }

        # If the character buffer is empty, update our prompt and such
        if ( !$char ) {

            # Set our init to 0

            $init = '0';
            $f = $rig->get_freq();
            ( $mode, $width ) = $rig->get_mode();

            $extra = '';
            # If we know this frequency, add it to the prompt
            $freqname = name_from_freq($f);
            if ( defined($freqname) && $freqname ne '' ) {
                $extra = '(' . $c_c . $freqname . $r . ') ';
            }

            $extra .= $r . '(Auto Mode)';
            ( $prompt, $tunertext, $extratext, $bandtext )
                = create_prompt($extra);

            %freqtext = freq_text();

            $textmode = Hamlib::rig_strrmode($mode);

            # If the mode matches the bypassdatamode expression, don't switch modes
            unless ( defined($bypassdatamode)
                && $bypassdatamode ne ""
                && $textmode =~ /$bypassdatamode/xms )
            {
                auto_mode_set( $f, $textmode, $freqtext{cwmatch},
                    $freqtext{datamatch}, $freqtext{ssbmatch} );
            }

            # Allow changing power on a 'channel', but autoset it
            # if frequency has changed.  Works from changing dial
            # or wsjtx etc, but not freq up/down in script in auto
            # mode, maybe a bug to fix
            if ( $autopower && $lastfreq != $f )
            {
                check_freq_power();
            }
        }

        # 100mS delay just so we're not beating up on rigctl too hard
        select( undef, undef, undef, $autodelay );
    }
    print "\nExiting back to normal mode\n";
    ReadMode('normal');
    $automode = '0';

    return;
}

# Scan the band.  If we're in auto mode when we start, we'll do a band scan
# if we started in band, otherwise we scan until stopped.  In manual mode,
# we scan the range specified
sub scan {
    my ( $f, $top, $direction ) = @_;
    my $scanchar = '';
    my $loops    = '0';
    my $bottom;

    # If we start scanning out of band, don't attempt to band scan
    my $scanstart      = '1';
    my $startoutofband = '0';

    $scanmode = '1';

    # If we're in range scan mode, scan up from the bottom
    if ($top) {
        $direction = 'up';

        # If we're range scanning, set radio to bottom
        $bottom = $f;
        parse_f($bottom);
    }

    while ( defined($scanchar) && $scanchar eq "" ) {

        # If we're in range scan mode, check boundaries
        if ($top) {
            my $ftmp = $rig->get_freq();
            if ( ( $ftmp / $freqdiv ) >= $top ) {
                if ( $direction eq 'up' ) { $direction = 'down'; }
            }

            if ( ( $ftmp / $freqdiv ) <= $bottom ) {
                if ( $direction eq 'down' ) { $direction = 'up'; }
            }
        }

        # If we were in Auto mode, do a band scan.  If we started out of band,
        # just keep going.
        if ($automode) {
            my %freqtext = freq_text();
            if ( $scanstart == 1 ) {
                $scanstart = '0';
                if ( $freqtext{outofband} ) {
                    $startoutofband = '1';
                }
            }

            if ( $freqtext{outofband} && $startoutofband == 0 ) {
                if    ( $direction eq 'up' )   { $direction = 'down'; }
                elsif ( $direction eq 'down' ) { $direction = 'up'; }
            }
        }

        ReadMode('cbreak');
        $scanchar = ReadKey(-1);

        # We need this defined, so if it's not, make it so
        if ( !defined($scanchar) ) {
            $scanchar = '';
        }

        # We need about 3 loops before we flush the char buffer
        if ( $loops < 3 ) { $scanchar = ''; }
        if ( $direction eq 'up' ) { $f += $scanstep; }
        if ( $direction eq 'down' ) { $f -= $scanstep; }
        parse_f($f);
        $loops++;

        # Every 10 loops, update the prompt
        if ( $loops % 10 == 0 ) {
            print $clearscreen;
            my ( $prompt, $tunertext, $extratext, $bandtext )
                = create_prompt();

            print $topleft;
            print $cl . $tunertext . ' ' . $extratext . "\n";
            print $cl . $prompt . "\r";
        }

        # If defined, pause a short period between frequencies
        if ( $scandelay > 0 ) {
            select( undef, undef, undef, $scandelay );
        }
    }

    ReadMode('normal');

    print $clearscreen . $topleft;
    $scanmode = '0';

    # Last our last known frequency; where we finished scanning
    $lastfreq = $f * $freqdiv;

    return;
}

sub create_prompt {
    no warnings 'uninitialized';
    my ($extra) = @_;

    # If we got nothing passed in, set $extra to blank
    if ( !defined($extra) ) {
        $extra = '';
    }

    my $freqcolor = $c_c;

    my $swrcolor = $c_c;

    my %freqtext    = freq_text();
    my $pretty_freq = $freqtext{pretty_freq};

    my ( $mode, $width ) = $rig->get_mode();
    my $textmode = Hamlib::rig_strrmode($mode);
    my $vfo      = $rig->get_vfo();
    my $textvfo  = Hamlib::rig_strvfo($vfo);
    $textvfo =~ s/MEM/M/;
    $textvfo =~ tr/ABM\.//cd;
    $lastvfo = $textvfo;

# Sometimes we seem to get nonsense, set pretty_freq to 0 to avoid ugly errors
    if ( $pretty_freq eq '' || $pretty_freq =~ /NaN/ ) {
        $pretty_freq = 0;
    }

    # Only store A/B, don't store M
    if ( $textvfo ne 'M' ) { $lastvfo = $textvfo; }
    my $lockstatus = $c_g . 'U';
    if ($locked) { $lockstatus = $c_r . 'L'; }

    if ($scanmode) {
        $extra = '(Scanning) ' . $extra;
    }

    if ( $freqtext{bandtext} ne '' ) {
        $extra = $extra . ' ' . $freqtext{bandtext};
    }

    if ( $freqtext{outofband} ) {
        $freqcolor = $c_r;
        $extra = $r . $c_r . $cl . 'Warning -- Out of Band ' . $r . $extra;
    }

    # Retrieve the output power of the rig and round up
    my $outputpower = get_power();

    my $swr = get_swr($pretty_freq);

    # If SWR is "NA" don't colorize it
    if ( $swr eq 'NA' ) {
        $swrcolor = $c_w;
    }

    # If SWR is over 2.5, change it to red
    if ( $swr ne 'NA' && $swr > 2.5 ) {
        $swrcolor = $c_r;
    }

    $signal = $rig->get_level_i($Hamlib::RIG_LEVEL_STRENGTH);

    if ( $avgsignal && $automode ) {
        push( @signalarray, $signal );
        splice( @signalarray, 0, -$avgsamples );
        $signal = average( $signal, @signalarray );
    }

    # Pad some stuff
    $pretty_freq = sprintf( '%8s', $pretty_freq );
    $textmode    = sprintf( '%3s', $textmode );
    $signal      = sprintf( '%3s', $signal );

    my $prompt = '';

    if ( $pretty_freq < ( $tune_bottom / 100 ) )  {
        $prompt = '(' . $c_r . 'Unknown; hamlib issue?' . '[' . $pretty_freq . '] ' . $r . ')';
    }
    else {
        $prompt
            = '('
            . $freqcolor
            . $pretty_freq
            . $r . '/'
            . $c_y
            . $signal
            . $r . '/'
            . $c_g
            . $textmode
            . $r . '/'
            . $c_y
            . $outputpower . 'w'
            . $r . '/'
            . $swrcolor
            . $swr
            . $r . '/'
            . $c_m
            . $textvfo
            . $r . '/'
            . $lockstatus
            . $r . ')';
    }
    return $prompt, $freqtext{tunertext}, $extra, $freqtext{bandtext},
        $freqtext{chantext};
}

# Set the mode based on the frequency we're on when in auto mode
sub auto_mode_set {
    my ( $f, $textmode, $cwmatch, $datamatch, $ssbmatch ) = @_;
    my $modefreq = '10000000';

    # Only SSB
    if ($ssbmatch) {
        if ( $lastmode eq 'c' ) {

            # Kludge to keep us on the same frequency we just tuned to
            my $freq = $rig->get_freq();
            $freq += $cwoffset;
            my $vfo = $rig->get_vfo();
            if ( $vfo == 1 ) {
                $rig->set_freq( $Hamlib::RIG_VFO_A, $freq );
            }
            if ( $vfo == 2 ) {
                $rig->set_freq( $Hamlib::RIG_VFO_B, $freq );
            }
        }

        # Handle 60m; it's always USB for SSB
        if ( grep( /$f/xms, @sixtymfreqs ) ) {
            if ( $textmode ne 'USB' ) {
                parse_mode('u');
            }
        }
        elsif ( $f < $modefreq && $textmode ne 'LSB' ) {
            parse_mode('l');
        }
        elsif ( $f > $modefreq && $textmode ne 'USB' && $textmode ne 'FM' ) {
            parse_mode('u');
        }

        $lastmode = '';
    }

    # Skip the rest if we're not auto changing modes
    if ($allmodeset) {

        # Only CW
        if ( $cwmatch && !$ssbmatch ) {
            unless ( $textmode eq 'CW' ) {

                # Kludge to keep us on the same frequency we just tuned to
                my $freq = $rig->get_freq();
                $freq -= $cwoffset;
                my $vfo = $rig->get_vfo();
                if ( $vfo == 1 ) {
                    $rig->set_freq( $Hamlib::RIG_VFO_A, $freq );
                }
                if ( $vfo == 2 ) {
                    $rig->set_freq( $Hamlib::RIG_VFO_B, $freq );
                }

                parse_mode('c');
                $lastmode = 'c';
            }
        }

        # Only Data
        if ( !$ssbmatch && !$cwmatch && $datamatch ) {
            unless ( $textmode eq 'DATA' ) {
                parse_mode('d');
            }
        }
    }

    return;
}

sub parse_mode {
    my ($input) = @_;
    $input = lc($input);

    my ( $mode, $width ) = $rig->get_mode();
    my $textmode = Hamlib::rig_strrmode($mode);

    my $output = '';

    if ( $input =~ /u/xms ) {
        $output = 'USB';
        if ( $textmode eq $output ) { return; }
        $rig->set_mode( $Hamlib::RIG_MODE_USB, $ssb_passband );
    }
    if ( $input =~ /l/xms ) {
        $output = 'LSB';
        if ( $textmode eq $output ) { return; }
        $rig->set_mode( $Hamlib::RIG_MODE_LSB, $ssb_passband );
    }
    if ( $input =~ /c/xms ) {
        $output = 'CW';
        if ( $textmode eq $output ) { return; }
        $rig->set_mode( $Hamlib::RIG_MODE_CW, $cw_passband );
    }
    if ( $input =~ /d/xms ) {
        $output = 'DATA';
        if ( $textmode eq $output ) { return; }
        $rig->set_mode( $Hamlib::RIG_MODE_PKTUSB, $data_passband );
    }
    if ( $input =~ /du/xms ) {
        $output = 'DATA';
        if ( $textmode eq $output ) { return; }
        $rig->set_mode( $Hamlib::RIG_MODE_PKTUSB, $data_passband );
    }
    if ( $input =~ /dl/xms ) {
        $output = 'DATA';
        if ( $textmode eq $output ) { return; }
        $rig->set_mode( $Hamlib::RIG_MODE_PKTLSB, $data_passband );
    }
    if ( $input =~ /am/xms ) {
        $output = 'AM';
        if ( $textmode eq $output ) { return; }
        $rig->set_mode( $Hamlib::RIG_MODE_AM, $am_passband );
    }
    if ( $input =~ /fm/xms ) {
        $output = 'FM';
        if ( $textmode eq $output ) { return; }
        $rig->set_mode( $Hamlib::RIG_MODE_FM, $fm_passband );
    }

    unless ( $automode || $scanmode || $locked ) {
        print "Switching to $output mode\n";
    }

    return;
}

sub parse_f {
    my ($freq) = @_;

    my $f = $rig->get_freq();

    my $cleanfreq = clean_freq($freq);

    if ( $f == $cleanfreq || $cleanfreq == 0 ) { return; }

    my $freqname = name_from_freq($cleanfreq);

    if ( $cleanfreq > 0 ) {

        # Auto set mode if manually changing frequency
        # We need to pass it the frequency here so it doesn't use the one
        # on the radio since we're not setting it until later.
        # TODO: Make sure this is sane.
        if ($fautomodeset) {
            my %freqtext = freq_text($cleanfreq);

            my ( $mode, $width ) = $rig->get_mode();
            my $textmode = Hamlib::rig_strrmode($mode);

            # Pass our new freq and invalid mode so we switch properly
            auto_mode_set( $cleanfreq, 'X', $freqtext{cwmatch},
                $freqtext{datamatch}, $freqtext{ssbmatch} );
        }

        my $prettyfreq = $cleanfreq / $freqdiv;
        unless ( $locked || $automode || $scanmode ) {
            print 'Switching to ' . $prettyfreq . ' kHz';
            if ( defined($freqname) && $freqname ne '' ) {
                print ' (' . $c_c . $freqname . $r . ')';
            }
            print "\n";
        }

        # We're changing frequency, update our $lastfreq before we do
        $lastfreq = $cleanfreq * $freqdiv;
        my $vfo     = $rig->get_vfo();
        my $textvfo = Hamlib::rig_strvfo($vfo);
        if ( $vfo == 1 ) {
            $rig->set_freq( $Hamlib::RIG_VFO_A, $cleanfreq );
        }
        if ( $vfo == 2 ) {
            $rig->set_freq( $Hamlib::RIG_VFO_B, $cleanfreq );
        }

        # If in Memory mode, switch to our last known VFO first
        if ( $textvfo eq 'MEM' ) {
            $rig->set_vfo($Hamlib::RIG_VFO_VFO);

            # If we were last in memory mode, default to VFO A
            if ( $lastvfo && ( $lastvfo eq 'A' || $lastvfo eq 'M' ) ) {
                $rig->set_vfo($Hamlib::RIG_VFO_A);
                $rig->set_freq( $Hamlib::RIG_VFO_A, $cleanfreq );
            }
            if ( $lastvfo && $lastvfo eq 'B' ) {
                $rig->set_vfo($Hamlib::RIG_VFO_B);
                $rig->set_freq( $Hamlib::RIG_VFO_B, $cleanfreq );
            }
        }
    }
    else {
        print "Invalid or blank frequency\n";
    }

    return $cleanfreq;
}

# Clean up, figure out what's frequency is probably wanted and return in hz
sub clean_freq {
    my ($freq) = @_;

    # Strip out anything but numbers and separators
    $freq =~ tr/0-9\.,//cd;

    # Normalize , separator to .
    $freq =~ s/,/\./xsm;

    # Bail out if we find multiple separators
    if ( $freq =~ /^\d+\.\d+\.\d+$/xsm ) {
        print "Multiple separators found; invalid input\n";
        return 0;
    }

    # ##.### looks like megahertz
    if ( $freq =~ /^\d{1,2}\.\d+$/xsm ) {
        $freq *= $freqdiv * $freqdiv;
    }

    # Otherwise, we assume input in Kilohertz
    else {
        $freq *= $freqdiv;
    }

    # Check to see if we're outside of tuning range
    if (   ( defined($tune_bottom) && defined($tune_top) )
        && ( $freq < $tune_bottom || $freq > $tune_top ) )
    {
        print 'Invalid input, outside of radio limits: ' . $freq . "hz\n";
        if ($enforce_tune_limit) { return 0; }
    }

    return $freq;
}

# Change a setting (TODO; not used yet)
sub change_setting {
    my ( $var, $setting ) = @_;
    print "[$var] [$setting]\n";

    #$var  =~ s/(\$\w+)/$1/eeg;
    my $newvar = \$var;
    print "scanstep: $scanstep\n";
    print '['
        . $var
        . '] is currently set to ['
        . $newvar . ' ' . ' '
        . $$newvar . ']' . "\n";
    $$var = $setting;
    print "scandstep: $scanstep\n";
    return 0;
}

# Give back a name if this frequency is known
sub name_from_freq {
    my ($freq) = @_;

    # If set, add frequency name to status text
    for my $key ( keys %freqnames ) {
        my $value = $freqnames{$key};
        my ( $keyvalue, undef ) = split( '\|', $key );
        if ( $freq eq $keyvalue ) {
            return $value;
        }
    }

    return;
}

# Give back a frequency if the channel name is known
sub freq_from_name {
    my ($channel)     = @_;
    my @channelarray  = ();
    my $channelsfound = 0;

    # Iterate through our freqnames hash looking for matching channels
    # and store them in @chanmatch
    for my $key ( keys %freqnames ) {
        my $value = $freqnames{$key};
        $value   = lc($value);
        $channel = lc($channel);

        if ( $value =~ /\Q$channel/xms ) {
            push( @channelarray, $value );
        }

        # Sort the array for humans
        @channelarray = sort(@channelarray);

        # Find out how many channel matches we found
        $channelsfound = @channelarray;
    }

    # If we matched at least one channel, figure out the next best channel
    # to switch to.
    if ($channelsfound) {
        my $preindex            = 0;
        my $currentchannelindex = -1;
        my $channelmatched      = 0;
        my $index               = 0;
        my $newchannel          = '';

        # First, find out where we are in our array
        foreach (@channelarray) {
            my $preindexfoundchannel = $_;

            if ( $preindexfoundchannel eq $lastchannel ) {
                $currentchannelindex = $preindex;
            }
            else {
                $preindex++;
            }
        }

        # Now that we know where our current channel is in the array, interate
        # through the array until we reach the next channel
        foreach (@channelarray) {
            my $foundchannel = $_;

            unless ($channelmatched) {

               # If the array index is above the current channel, switch to it
                if ( $index > $currentchannelindex ) {
                    $newchannel = $foundchannel;
                    $channelmatched++;
                }
                else {
                    $index++;
                }

                # If we're at the end of the array, change to the first match
                if ( $index >= $channelsfound ) {
                    $newchannel = $channelarray[0];
                    $channelmatched++;
                }
            }

            # Change to the channel we identified to go to
            for my $key ( keys %freqnames ) {
                my $value = $freqnames{$key};
                $value   = lc($value);
                $channel = lc($channel);
                my $mode = '';

                if ( $value =~ /\Q$newchannel/xms ) {
                    $lastchannel = $value;

                    # If a specific mode has been specified, split it out
                    if ( $key =~ /\|/xms ) {
                        ( $key, $mode ) = split( '\|', $key );
                    }
                    my $returnfreq = $key / $freqdiv;
                    return ( $returnfreq, $mode );
                }
            }
        }
    }
    else {
        if ( $channel ne "" ) {
            print 'Unknown channel:  ' . $channel . "\n";
        }
    }

    # If no channel passed, print our known channels
    if ( $channel eq "" ) {
        for my $key ( keys %freqnames ) {
            my $value = $freqnames{$key};
            $value   = lc($value);
            $channel = lc($channel);
            print $value . "\n";
        }
    }

    return;
}

# Get radio output power
# TODO:  How to handle power changes directly on the radio
sub get_power {
    my $outputpower
        = int(
        ( $rig->get_level_f($Hamlib::RIG_LEVEL_RFPOWER) * $powerdivider )
        + .5 );

        # If our current output power does not match lastpower and freqpower
        # isn't set, set lastpower to current power to handle manual radio power adjustment
        if (defined($lastpower) && $outputpower != $lastpower && $freqpower != 1)
        {
            $lastpower = $outputpower;
        }
    return $outputpower;
}

# Get radio swr.  Constants and multiplier set in module
sub get_swr {

    my ($freq) = @_;

    my $swr = 'NA';
    my $tx_flag = '0';
    
    $tx_flag = '1' if $rig->get_ptt();

    if ( $tx_flag )
    {
        # Get the SWR from the radio
        my $swr_level = $rig->get_level_f($Hamlib::RIG_LEVEL_SWR);
        $swr = sprintf( '%.1f', ($swr_level + $swr_constant) / $swr_multiplier );

        # If swr_testing is set, output the raw value rather than the calculated one
        if ($swr_testing)
        {
            $swr = sprintf( '%.6f', $swr_level );
        }

        $lastswr = $swr;
        # If swr_logging is set, log the current swr
        log_swr($freq,$swr) if $swr_logging;
    }
    else
    {
        $swr = $lastswr;
    }

    return $swr;
}

# Set radio output power
sub set_power {
    my ( $power, $setlastpower ) = @_;
    $power =~ s/w//i;
    my $adjustedpower = $power / $powerdivider;
    $rig->set_level( $Hamlib::RIG_LEVEL_RFPOWER, $adjustedpower );

# If $setlastpower is set, set the last power value
# We want to do this when manually setting power, or resetting power after
# previously being automatically adjusted but not when initially automatically
# adjusted
    if ($setlastpower) {
        $lastpower = $power;
    }
}

# Check to see if this frequency demands a specific power output
sub check_freq_power {
    my $freq        = $rig->get_freq();
    my $freqmatched = '0';
    my $power       = get_power();

    # If frequency matches, set power level
    for my $key ( keys %powerfreqs ) {
        if ( $key eq $freq ) {
            $freqmatched++;
            $freqpower = '1';
            my $newpower = $powerfreqs{$key};
            if ( $power != $newpower ) {
                $lastpower = $power;
                my @powerdata = ( $newpower, 0 );
                set_power(@powerdata);
            }
        }
    }

    # If this frequency did not match and our power was previously changed set it back
    if ( $freqmatched == 0 && $power != $lastpower ) {
        $freqpower = '0';
        my @powerdata = ( $lastpower, 1 );
        set_power(@powerdata);
    }
}

# Log SWR information about this frequency
sub log_swr {
    my ($freq,$swr) = @_;
    
    # If our frequency has not changed, return
    if ($freq == $lastswrfreq)
    {
        return;
    }

    $lastswrfreq = $freq;

    if ( defined($swrfile) && $swr > 0 ) {
        open 'SWRFILE', '>>', $swrfile or do {
            print 'Can\'t write to ' . $swrfile . "\n";
            return;
        };
        my $timestamp   = logtimestamp();
        print SWRFILE $timestamp . ' - ['
            . $freq . '] ['
            . $swr . '] '
            . "\n";
        close('SWRFILE');

        return;
    }
}

# Save a note about this frequency
sub save_note {
    my ($note) = @_;
    if ( defined($notefile) && length($note) > 0 ) {
        open 'NOTEFILE', '>>', $notefile or do {
            print 'Can\'t write to ' . $notefile . "\n";
            return;
        };
        my $timestamp   = logtimestamp();
        my $f           = $rig->get_freq();
        my $pretty_freq = $f / $freqdiv;
        my ( $mode, $width ) = $rig->get_mode();
        my $textmode = Hamlib::rig_strrmode($mode);
        print NOTEFILE $timestamp . ' - '
            . $pretty_freq . ' ['
            . $textmode . '] '
            . $note . "\n";
        close('NOTEFILE');

        return;
    }
}

# Get a pretty timestamp for our logfile
sub logtimestamp {
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )
        = localtime(time);
    my $nice_timestamp = sprintf(
        '%04d' . '-' . '%02d' . '-' . '%02d' . ' ' . '%02d' . ':' . '%02d'
            . ':' . '%02d',
        $year + 1900,
        $mon + 1, $mday, $hour, $min, $sec
    );

    return $nice_timestamp;
}

sub usage {
    my ($exted) = @_;

    if ( $exted eq '?' ) {
        my $helptext = <<'END';
Manual mode commands

auto - Switch to autotune mode
   f - Switch to frequency in kHz
   u - Switch to Upper Side Band
   l - Switch to Lower Side Band
   c - Switch to CW
  am - Switch to AM
  fm - Switch to FM
   d - Switch to USB Data Mode
  dl - Switch to LSB Data mode
   a - Switch to VFO A
   b - Switch to VFO B
   r - Revert to last freq/mode
   q - Exit autotune mode or exit
   ! - Repeat last command
   ? - Help
  ?? - Auto mode help
tuner - on/off/tune
lock - Lock to current freq/mode
unlock - Unlock
chan name - Switch to frequency of named channel
note - Capture a note about the current frequency
f7188lb - Move to 7188kHz LSB on VFO B
28450 - Move to 28450kHz (assumes input in kilohertz)
28203.5 or 28.2035 - Move to 28203.5kHz (only way to input sub-khz frequencies)
s7200-7300 - Scan 7200 to 7300

(freq/sig/mode/lockstatus)
END
        print $helptext;
    }

    if ( $exted eq '??' ) {
        my $morehelptext = <<'END';
Auto mode commands

f - Prompt for frequency
p - Prompt for power setting
(space) - Toggle PTT
t - Toggle tuner on/off
T - Tune radio
n - Prompt for note input
q - Exit auto mode
Q - Exit script
l - Lock frequency/mode
u - Unlock
Shift Up/Shift Down - Power Up/Down
Cursor right/left - +/- 10hz
Cursor up/down - +/- 1khz
Page up/down - +/- 5khz
Home/End - +/- 10khz
Shift PgUp/Shift PgDown - Scan Up/Scan Down
END
        print $morehelptext;
    }

    return;
}

# Set up our color tags
sub color_tags {

    # If we're colorizing, set our bold default
    if ( $coloroutput && $ansi ) {
        $r = RESET();

        $bold = BOLD();
        # Don't use bright colors on a light terminal theme
        if ($lightterm) {
            $c_r = RED();
            $c_g = GREEN();
            $c_c = CYAN();
            $c_m = MAGENTA();
            $c_b = BLUE();
            $c_y = YELLOW();
            $c_w = WHITE();
        }
        else {
            $c_r = BRIGHT_RED();
            $c_g = BRIGHT_GREEN();
            $c_c = BRIGHT_CYAN();
            $c_m = BRIGHT_MAGENTA();
            $c_b = BRIGHT_BLUE();
            $c_y = BRIGHT_YELLOW();
            $c_w = BRIGHT_WHITE();
        }
    }

    # Go to start of line, clear entire line
    $cl = "\r\033[2K";

    # Clear from cursor to the end of the screen
    $clearrestscreen = "\033[J\r";

    # Clear the entire screen
    $clearscreen = "\033[2J";

    # Move cursor to the 0,0 position
    $topleft = "\033[0;0H";

    return;
}

# Create an psuedo smeter
sub smeter
{
    my ( $level ) = @_;

    no warnings 'uninitialized';

    my $metertext;

    $metertext .=        '▁▁▁' if $level > -55; # This is about S1 on my FT450
    $metertext .= $c_g . '▂▂▂' if $level > -35; # S3
    $metertext .= $c_c . '▃▃▃' if $level > -20; # S5
    $metertext .= $c_b . '▄▄▄' if $level > 0;   # S7
    $metertext .= $c_r . '▅▅▅' if $level > 15;  # S9
    $metertext .= $c_m . '▆▆▆▆▆' if $level > 35;  # S9 + 20
    $metertext .= $c_m.  '▒▒▒▒▒' if $level > 45;  # S9 + 40
    $metertext .= $c_b . '██████' if $level >= 60;  # S9 + 60

    $metertext .= $r;

    my $metergraph = ' ';
    $metergraph .= "\n" . '| 1| 3| 5| 7| 9| +20| +40| +60|' if $smetergraph;

    return $metertext . $metergraph;
}

# Manipulate the tuner in the radio
sub tuner 
{
    my ($op) = @_;

    my $returnstring = '';

    my $tunerenabled = '0';
    # Yuck, why do have to do some of this with rigctl
    my $result = `echo u TUNER | rigctl -m 2 | grep Func`;
    if ( $result =~ /Rig command: Func Status: (\d+)/ )
    {
        $tunerenabled = $1;
    }

    # Unsure if this actually does anything or not??
    my $flag = $rig->get_level_i($Hamlib::RIG_FLAG_TUNER);
    # Assuming it is, >0 means we can do something
    #print "[$op] [$flag]\n";
    if ( $flag )
    {
        #print "flag: [$flag]\n";
        my $vfo = $rig->get_vfo();

        if ( $op eq 'toggle' || $op eq 'on' || $op eq 'off' )
        {
            #my $test = $rig->get_func($vfo, $Hamlib::RIG_FLAG_TUNER);
            #my $test = $rig->has_set_func($Hamlib::RIG_FLAG_TUNER);
            #print "toggle [$test]\n";
            if ( $tunerenabled || $op eq 'off' )
            {
                #print "disabling\n";
                #$rig->set_func($vfo, $Hamlib::RIG_FUNC_LOCK, 0);
                $tunerenabled = '0';
                $returnstring = 'Disabled tuner';
            }
            else
            {
                #print "enabling  \n";
                #$rig->set_func($vfo, $Hamlib::RIG_FUNC_LOCK, 1);
                $tunerenabled = '1';
                $returnstring = 'Enabled tuner';
            }
            my @args = ("echo U TUNER $tunerenabled | rigctl -m 2 >/dev/null");
            system(@args);
        }        

        if ( $op eq 'tune' )
        {
            #print "tune\n";
            # This is not working
            $rig->set_func($vfo, $Hamlib::RIG_FUNC_TUNER, 1);

            # Make sure the tuner is enabled; probably not needed
            my @args = ("echo U TUNER $tunerenabled | rigctl -m 2 >/dev/null");
            system(@args);

            # This DOES work
            $rig->vfo_op($vfo, $Hamlib::RIG_OP_TUNE);

            $returnstring = 'Tuned radio';
        }
    }
    
    return $returnstring;
}

# Average a passed-in array
sub average {
    my ( $signal, @array ) = @_;
    my $intval = '0.5';

    my $arraycount = @array;
    unless ($arraycount) { return $signal; }

    # First time through, set to the first signal we see
    if ( $lastavg == -99 ) { $lastavg = $signal; }

    if ( $avgtimes >= $avgsamples ) {
        my $sum;
        foreach my $item (@array) {
            $sum += $item;
        }
        $avgtimes = '0';
        $signal   = int( $sum / @array + .5 );
        if ( $signal < 0 ) { $intval *= -1; }
        $lastavg = $signal;
    }
    else {
        $avgtimes++;
        $signal = $lastavg;
    }

    return $signal;
}

# Open the hamlib connection
sub rigopen {
    if ( $rigopens > $rigopenmax ) {
        #$autoloop = '0';
        # Try to slow the loop, not stop it entirely
        sleep 1;
    }
    $rigopens++;
    $rig->open();

    return;
}

# Close the hamlib connection
sub rigclose {
    $rig->close();

    return;
}
