package rigtool;

# URL for Hamlib
our $hamliburl = "localhost:4532";

# Your call priviledges (sorry, US-only for now, blank this out for elsewhere)
our $country = 'USA';
our $license = 'Advanced';

# Note file to use
# Location of file which will pause scanning
my $home = $ENV{"HOME"};
our $notefile = $home . '/rigtoolnotes.txt';

# Pass band widths
our $cw_passband   = '1000';
our $data_passband = '3000';
our $ssb_passband  = '3000';
our $am_passband   = '6000';
our $fm_passband   = '12000';

# Radio tuning limits
our $tune_bottom        = '3000';
our $tune_top           = '56000000';
our $enforce_tune_limit = '0';

# Power divider.  Example, if I want 15W, I want it to set the power to .06, which is 15/255
our $powerdivider = '255';

# SWR calculation for me is a linear formula I used wolfram alpha to determine
# by using my radios meter, adjusting my tuner for an SWR of 1, 2, and 3 and for
# each writing down the raw value given to me.  Plug those into wolfram, and it
# outputs a formula.  Here is my FT-450 as an example.  To get the raw value, set
# swr_testing to 1.

# Every radio is probably different!

## new; hamlib has changed?  No reading/SWR 1.1 is now '1' not 0

# 1.0 = 1.0
# 2.0 = 1.52083301544189
# 3.0 = 2.66666698455811
# 0.833333 x + 0.0624997

## old
# 1.0 = 0.03137
# 2.0 = 0.25882
# 3.0 = 0.48325

# wolfram fit | {0.03137, 0.25882, 0.48325}
# Outputs: 0.22594 x - 0.194067 (linear)
# Solve for x (SWR), which results in
# swr = (value + 0.194067) / 0.22594
# or, expanded:  swr = 4.42595 * value + 0.858932

our $swr_multiplier = '0.833333';
our $swr_constant = '-0.0624997';
our $swr_testing = '0';

# If you want to log SWR info to a file, set this to 1 and set the filename
our $swr_logging = '1';
our $swrfile = $home . '/rigtoolswr.txt';

# If your radio offsets when it switches between CW and SSB, you can set that here
# or set it to 0 if you don't want the script twiddling this on CW/SSB transitions
our $cwoffset = '700';

# If set to 1, execute auto_mode_set on manual frequency change.  cw/ssb switch
# depends on $allmodeset setting
our $fautomodeset = '1';

# If specified and the radio is set to this mode, do not force change modes if
# auto_mode_set is used.  This prevents things like WSJT-X switching to Data mode
# for JT65 and the script changing it back to USB.
our $bypassdatamode = 'PKT.SB';

# If set to 0, do not switch between cw and ssb, but still sets lsb/usb
# 1 executes auto_mode_set and switches beween cw and ssb as well as sets lsb/usb
# and in the future perhaps data modes as well
our $allmodeset = '0';

# Average the last N signals or not
our $avgsignal = '1';

# How many samples to average
our $avgsamples = '5';

# Determine if we show tuneinfo or not.  If you have an autotuner or do not need to
# manually tune you can just set this to 0.  If you do want this information, you
# will need to modify the %tuneinfo variable in the get_bandplan function
our $showtuneinfo = '1';

# Determine if we show band info or not (ie: what band we're on)
our $showbandinfo = '1';

# Lock the mode (set it back if it's changed on the radio)
# Really only works in auto mode (?)
our $locked = '0';

# How many times can we fail opening the port before we give up?
our $rigopenmax = '25';

# Step sizes for cursor keys in auto mode
our $hzstep      = '0.1';
our $khzstep     = '1.0';
our $fivekhzstep = '5.0';
our $largestep   = '10.0';

# These keycodes may very well vary based on terminal program used
# Get these keycodes using showkey -a
# we only get the last two keys and evaluate them, so "^[[5~" would be "5~"

# Step keys
our $hzupkey        = 'C'; # right-arrow
our $hzdownkey      = 'D'; # left-arrow
our $khzupkey       = 'A'; # up-arrow
our $khzupkeytwo    = '[A'; # scroll up
our $khzdownkey     = 'B'; # down-arrow
our $khzdownkeytwo  = '[B'; # scroll down
our $fivekhzupkey   = '5~'; # pg-up
our $fivekhzdownkey = '6~'; # pg-down
our $largeupkey     = '1~'; # home
our $largedownkey   = '4~'; # end

# Power keys
our $powerupkey   = '2A'; # shift-up
our $powerdownkey = '2B'; # shift-down

# Power increment
our $powerinc     = '5';

# Scan keys
our $scanupkey   = '5;'; # shift-PgUp
our $scandownkey = '6;'; # shift-PgDown

# Step size for scan mode
our $scanstep = '.5';

# Scan delay, in seconds.  .1 for 100mS
our $scandelay = '0';

# Auto mode delay, in seconds
our $autodelay = '0.1';

# If you want to have color output, set this to 1
our $coloroutput = '1';

# If you're using a dark terminal background, set light = 0
our $lightterm = '0';

# Show s-meter
our $smeter = '1';
our $smetergraph = '1';

# Auto set power in Auto mode
our $autopower = '1';
# Auto set power in Manual mode
our $manualpower = '0';

# Define 60M for our bandplan
our @sixtymfreqs = ( '5330500', '5346500', '5357000', '5371500', '5403500' );

# Define our (optional) tuner info (9/15/2022)
our %tuneinfo = (
    '135700-137800'     => 'NA',           # 2200M
    '472000-479000'     => 'NA',           # 630M
    '1800000-1900000'   => 'A 5.0 6.0',    # 160m
    '1900000-1950000'   => 'A 5.0 5.0',    # 160m
    '1950000-2000000'   => 'A 5.0 4.5',    # 160m
    '3535000-3850000'   => 'ATU',          # 80m
    '3850000-4000000'   => 'L 5.2 1.1',    # 80m
    '5330500-5403500'   => 'K 3.0 2.0',    # 60m
    '7025000-7250000'   => 'ATU',          # 40m
    '7250000-7300000'   => 'I 4.5 1.6',    # 40m
    '10100000-10150000' => 'G 6.0 5.0',    # 30m
    '14025000-14200000' => 'ATU',          # 20m
    '14200000-14350000' => 'D 5.0 5.0',    # 20m
    '18068000-18168000' => 'C 0.0 2.5',    # 17m
    '21025000-21100000' => 'ATU',          # 15m
    '21100000-21450000' => 'B 3.9 3.2',    # 15m
    '24890000-24990000' => 'B 3.0 1.8',    # 12m
    '28000000-29700000' => 'ATU',          # 10m
    '50000000-54000000' => 'ATU'           # 6m
);

# Define our (optional) frequency names.  If the channel does not conform to
# the standard <10Mhz LSB rule (such as data modes), add the specific mode
# use after the frequency, such as 7070000|d
our %freqnames = (
    '136000|d'    => '2200M WSPR',
    '474200|d'    => '630M WSPR',
    '1840000|d'   => '160M FT8',
    '1838000|d'   => '160M JT65',
    '2500000|am'  => 'WWV 2MHZ',
    '5000000|am'  => 'WWV 5MHZ',
    '3573000|d'   => '80M FT8',
    '3576000|d'   => '80M JT65',
    '5330500'     => '60M CH1',
    '5346500'     => '60M CH2',
    '5357000'     => '60M CH3/JT65/FT8',
    '5371500'     => '60M CH4',
    '5403500'     => '60M CH5',
    '7070000|d'   => '40M PSK31',
    '7074000|d'   => '40M FT8',
    '7076000|d'   => '40M JT65',
    '7173000|d'   => '40M Digital SSTV',
    '10000000|am' => 'WWV 10MHZ',
    '10136000|d'  => '30M FT8',
    '10138000|d'  => '30M JT65',
    '10142150|d'  => '30M PSK31',
    '10147600|d'  => '30M APRS',
    '14070150|d'  => '20M PSK31',
    '14074000|d'  => '20M FT8',
    '14076000|d'  => '20M JT65',
    '14230000|d'  => '20M Analog SSTV',
    '14233000|d'  => '20M Digital SSTV',
    '15000000|am' => 'WWV 15MHZ',
    '18100000|d'  => '17M FT8',
    '18102000|d'  => '17M JT65',
    '20000000|am' => 'WWV 20MHZ',
    '21074000|d'  => '15M FT8',
    '21076000|d'  => '15M JT65',
    '24915000|d'  => '12M FT8',
    '24917000|d'  => '12M JT65',
    '25000000|am' => 'WWV 25MHZ',
    '28074000|d'  => '10M FT8',
    '28076000|d'  => '10M JT65',
    '50276000|d'  => '6M JT65 Old',
    '50310000|d'  => '6M JT65',
    '50313000|d'  => '6M FT8',
    '50125000'    => '6M Call',
    '50200000'    => '6M Call 200',
    '28400000'    => '10M Call',
    '7200000'     => '40M Lids',
    '3840000'     => '80M Lids',
    '8472000'     => 'WLO Marine',
);

# Define our (optional) band names
our %bandnames = (
    '135700-137800'     => '2200M',
    '472000-479000'     => '630M',
    '1800000-2000000'   => '160M',
    '3500000-4000000'   => '80M',
    '5330500-5403500'   => '60M',
    '7000000-7300000'   => '40M',
    '10100000-10150000' => '30M',
    '14000000-14350000' => '20M',
    '18068000-18168000' => '17M',
    '21000000-21450000' => '15M',
    '24890000-24990000' => '12M',
    '28000000-29700000' => '10M',
    '50000000-54000000' => '6M',
);

# Define frequencies which we want to force a power output on
# when changing to, but allows modification on frequency as needed
our %powerfreqs = (
    '1836600'  => '5',    # 160m WSPR
    '3568600'  => '5',    # 80m WSPR
    '7038600'  => '5',    # 40m WSPR
    '7074000'  => '35',   # 40m WSJTX
    '10138700' => '5',    # 30m WSPR
    '14074000' => '35',   # 20m WSJTX
    '14095600' => '5',    # 20m WSPR
    '18104600' => '5',    # 17m WSPR
    '21094600' => '5',    # 15m WSPR
    '24924600' => '5',    # 12m WSPR
    '28074000' => '35',   # 10m WSJTX
    '28124600' => '5',    # 10m WSPR
    '50293000' => '5',    # 6m WSPR
    '50313000' => '35',   # 6m WSJTX
    '50323000' => '35',   # 6m WSJTX
);

# Define our band plans
# datafreqs isn't really used right now, but define it.  Multiple flags can
# be active as there is usage overlap such a data/cw, ssb/cw etc
sub get_bandplan {
    my ( $bandcountry, $bandlicense ) = @_;
    $bandcountry = lc($bandcountry);
    $bandlicense = lc($bandlicense);

    my $privs = '0';

    # Build up priviledges from the base of novice.  This is of course based
    # on the US band plan.  Other band plans should be easily added and put
    # in their own country section

    if ( $bandcountry eq 'usa' ) {
        if ( $bandlicense eq 'novice' )     { $privs = '1'; }
        if ( $bandlicense eq 'technician' ) { $privs = '2'; }
        if ( $bandlicense eq 'general' )    { $privs = '3'; }
        if ( $bandlicense eq 'advanced' )   { $privs = '4'; }
        if ( $bandlicense eq 'extra' )      { $privs = '5'; }

        if ( $privs == 0 ) {
            print 'Unknown license ' . $bandlicense . "\n";
        }

        # Novice
        if ( $privs > 0 ) {
            my @newcwfreqs = (
                '3525000-3600000',      # 80m
                '7025000-7125000',      # 40m
                '21025000-21200000',    # 15m
                '28000000-28300000'     # 10m
            );

            my @newdatafreqs = ();

            my @newssbfreqs = (
                '28300000-28500000'     # 10m
            );

            push( @cwfreqs,   @newcwfreqs );
            push( @datafreqs, @newdatafreqs );
            push( @ssbfreqs,  @newssbfreqs );
        }

        # Technician
        if ( $privs > 1 ) {
            my @newcwfreqs = (
                '50000000-50100000'     # 6m
            );

            my @newdatafreqs = ();

            my @newssbfreqs = (
                '50100000-54000000'     # 6m
            );

            push( @cwfreqs,   @newcwfreqs );
            push( @datafreqs, @newdatafreqs );
            push( @ssbfreqs,  @newssbfreqs );
        }

        # General
        if ( $privs > 2 ) {
            my @newcwfreqs = (
                '135700-137800',        # 2200m
                '472000-479000',        # 630m
                '1800000-2000000',      # 160m
                '3525000-3600000',      # 80m
                '5330500', '5346500', '5357000', '5371500', '5403500',   # 60m
                '7025000-7125000',                                       # 40m
                '10100000-10150000',                                     # 30m
                '14025000-14150000',                                     # 20m
                '18068000-18110000',                                     # 17m
                '21025000-21200000',                                     # 15m
                '24890000-24930000',                                     # 12m
                '28000000-28300000'                                      # 10m
            );

            my @newdatafreqs = (
                '135700-137800',      # 2200m
                '472000-479000',      # 630m
                '1800000-2000000',    # 160m
                '3525000-3600000',    # 80m
                '5330500', '5346500', '5357000', '5371500', '5403500',   # 60m
                '7025000-7125000',                                       # 40m
                '10100000-10150000',                                     # 30m
                '14025000-14150000',                                     # 20m
                '18068000-18110000',                                     # 17m
                '21025000-21200000',                                     # 15m
                '24890000-24930000',                                     # 12m
                '28000000-28300000'                                      # 10m
            );

            my @newssbfreqs = (
                '1800000-2000000',    # 160m
                '3800000-4000000',    # 80m
                '5330500', '5346500', '5357000', '5371500', '5403500',   # 60m
                '7175000-7300000',                                       # 40m
                '10100000-10150000',                                     # 30m
                '14225000-14350000',                                     # 20m
                '18110000-18168000',                                     # 17m
                '21275000-21450000',                                     # 15m
                '24930000-24990000',                                     # 12m
                '28500000-29700000'                                      # 10m
            );

            push( @cwfreqs,   @newcwfreqs );
            push( @datafreqs, @newdatafreqs );
            push( @ssbfreqs,  @newssbfreqs );
        }

        # Advanced
        if ( $privs > 3 ) {
            my @newcwfreqs = ();

            my @newdatafreqs = ();

            my @newssbfreqs = (
                '3700000-3800000',      # 80m
                '7125000-7175000',      # 40m
                '14175000-14225000',    # 20m
                '21225000-21275000'     # 15m
            );

            push( @cwfreqs,   @newcwfreqs );
            push( @datafreqs, @newdatafreqs );
            push( @ssbfreqs,  @newssbfreqs );
        }

        # Extra
        if ( $privs > 4 ) {
            my @newcwfreqs = (
                '3500000-3525000',      # 80m
                '7000000-7025000',      # 40m
                '14000000-14025000',    # 20m
                '21000000-21025000'     # 15m
            );

            my @newdatafreqs = ();

            my @newssbfreqs = (
                '3600000-3700000',      # 80m
                '14150000-14175000',    # 20m
                '21200000-21225000'     # 15m
            );

            push( @cwfreqs,   @newcwfreqs );
            push( @datafreqs, @newdatafreqs );
            push( @ssbfreqs,  @newssbfreqs );
        }
    }

    return ( \@cwfreqs, \@datafreqs, \@ssbfreqs );
}

1;
